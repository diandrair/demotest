package com.example.demo.config;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WebDriverConfig {

	@Bean
	public WebDriver webDriver() {
		WebDriverManager.chromedriver().version("100.0.4896.60").setup();
		return new ChromeDriver();
	}

	@Bean
	public WebDriverWait wait(WebDriver driver) {
		return new WebDriverWait(driver, 30);
	}

}

