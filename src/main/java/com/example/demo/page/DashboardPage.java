package com.example.demo.page;

import com.example.demo.AbstractBasePage;
import com.example.demo.util.BrowserActions;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DashboardPage extends AbstractBasePage {

    @Autowired
    private BrowserActions browserActions;

    @FindBy(xpath = "//div[@class='ant-row']//span[contains(text(),'Dashboard')]")
    public WebElement dashboardText;

    @Override
    public boolean isAt() {
        return this.wait.until((d)->this.dashboardText.isDisplayed());
    }
}
