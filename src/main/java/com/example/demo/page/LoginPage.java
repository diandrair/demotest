package com.example.demo.page;

import com.example.demo.AbstractBasePage;
import com.example.demo.util.BrowserActions;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class LoginPage extends AbstractBasePage {

    @Autowired
    private BrowserActions browserActions;

    @FindBy(xpath = "//div[@class='custom-card-title']")
    private WebElement welcomeText;

    @FindBy(xpath = "//div[. = 'Select your company']")
    private WebElement selectCompanyField;

    @FindBy(xpath = "//li[. = ' PT. SATRIA PIRANTI PERKASA ']")
    private WebElement selectSPP;

    @FindBy(id = "signinForm_username")
    private WebElement usernameField;

    @FindBy(xpath = "//div[2]//div[2]//div[1]//div[1]")
    public WebElement usernameErrorMsg;

    @FindBy(id = "signinForm_password")
    private WebElement passwordField;

    @FindBy(xpath = "//div[3]//div[2]//div[1]//div[1]")
    public WebElement passwordErrorMsg;

    @FindBy(xpath = "//a[normalize-space()='here']")
    public WebElement forgotPassMenu;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement signInBtn;

    @FindBy(xpath = "//span[contains(text(),'Bad credentials')]")
    public WebElement badCredential;


    public void goTo() {
        this.driver.get("https://erp-dev.satriapiranti.co.id/");
        this.driver.manage().window().maximize();
    }

    public void setCompany (){
       browserActions.clickElement(selectCompanyField);
       browserActions.clickElement(selectSPP);
    }

    public void fillLoginForm (String username, String password){
        browserActions.sendKeysToElement(usernameField, username);
        browserActions.sendKeysToElement(passwordField,password);
    }

    public void clickSignIn (){
        browserActions.clickElement(signInBtn);
    }


    @Override
    public boolean isAt() {
        return this.wait.until((d)->this.welcomeText.isDisplayed());
    }
}