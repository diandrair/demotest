package com.example.demo.util;

import com.example.demo.AbstractBasePage;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.springframework.stereotype.Component;

@Component
public class BrowserActions extends AbstractBasePage {


    public void clickElement(WebElement element) {
        this.wait.until(ExpectedConditions.elementToBeClickable(element)).click();
    }

    public void sendKeysToElement(WebElement element, String keys) {
        this.wait.until(ExpectedConditions.visibilityOf(element)).sendKeys(keys);
    }

    public void setKeyboardKey(WebElement element, Keys key) {
        this.wait.until(ExpectedConditions.visibilityOf(element)).sendKeys(key);
    }

    public String getElementText(WebElement element) {
        return this.wait.until(ExpectedConditions.visibilityOf(element)).getText();
    }


    public void scrollIntoView(WebElement element){
        this.wait.until(ExpectedConditions.visibilityOf(element));
        JavascriptExecutor js = (JavascriptExecutor)driver;
        js.executeScript("arguments[0].scrollIntoView(true);", element);
    }

    public void clickJSE(WebElement element){
        JavascriptExecutor js = (JavascriptExecutor)driver;
        js.executeScript("arguments[0].click()", element);
    }

    public void acceptAlert() {
        driver.switchTo().alert().accept();
    }

    public void refresh() {
        driver.navigate().refresh();
    }

    @Override
    public boolean isAt() {
        return false;
    }
}
