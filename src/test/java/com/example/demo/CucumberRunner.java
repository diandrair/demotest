package com.example.demo;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.DataProvider;

@CucumberOptions(
        strict = true,
        features = "classpath:feature",
        glue = "com.example.demo.StepDefinitions",
        tags = { "@login"},
        plugin = {
                "pretty",
                "html:/Users/diandrair/Documents/SPPautomation/"
        }
)
public class CucumberRunner extends AbstractTestNGCucumberTests {

    @Override
    @DataProvider(parallel = false)
    public Object[][] scenarios() {
        return super.scenarios();
    }

}
