package com.example.demo.StepDefinitions;

import com.example.demo.page.DashboardPage;
import com.example.demo.page.LoginPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class LoginStepDef {

    @Autowired
    private LoginPage loginPage;

    @Autowired
    private DashboardPage dashboardPage;

    @Given("I am on login page")
    public void iAmOnLoginPage() {
        loginPage.goTo();
        loginPage.isAt();
    }

    @When("I select company")
    public void iSelectCompany() {
        loginPage.setCompany();
    }

    @And("I input {string} and {string}")
    public void iInputAnd(String username, String pass) {
        loginPage.fillLoginForm(username, pass);
    }

    @And("I click sign in button")
    public void iClickSignInButton() {
        loginPage.clickSignIn();
    }

    @Then("I can see dashboard page")
    public void iCanSeeDashboardPage() {
        dashboardPage.isAt();
    }
}
