Feature: Login
  @login
  Scenario Outline: login as an admin
    Given I am on login page
    When I select company
    And I input "<username>" and "<password>"
    And I click sign in button
    Then I can see dashboard page
    Examples:
      | username | password |
      | admin_erp| password |